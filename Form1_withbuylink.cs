using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyHttp.Http;
using CsQuery;
using System.Web;
using System.Text.RegularExpressions;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public string itemName, itemSize, link;
        IDomObject item;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            itemName = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            itemSize = textBox2.Text;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            itemName = textBox1.Text;
            var http = new HttpClient();
            var response = http.Get("https://www.off---white.com/en/RU/section/new-arrivals");
            //MessageBox.Show($"Name: {itemName}\nSize: {itemSize}");
            CQ html  = response.RawText;
            //Regex rgx = new Regex("/(?:\r\n|\r|\n)/g");
            html[".brand-name"].EachUntil(el =>
            {
                string currText = html[el].Text().Replace("\n", "");
                if (currText.ToLower().IndexOf(itemName.ToLower()) >= 0)
                {
                    richTextBox1.Text += $"{currText}\n";
                    item = el;
                    return false;
                
                }
                return true;
            });
            link = $"https://www.off---white.com{html[item].Closest("a").Attr("href")}"; // could be with .json
            string imageLink = html[html[item].Closest("figure").Children()[0]].Attr("src");
            string price = html[html[html[item].Parents()[0]].Find("strong")].Text();
            pictureBox1.Image = LoadImageWithReferer(imageLink, "https://www.off---white.com/en/RU");
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            label5.Text = html[item].Text();
            linkLabel1.Text = "Buy";
            label6.Text = price;
            linkLabel1.Links.Clear();
            linkLabel1.Links.Add(0,4,link);
            linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // richTextBox1.Text = response.Cookies.ToString();
        }
        private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            // Specify that the link was visited.
            this.linkLabel1.LinkVisited = true;

            // Navigate to a URL.
            System.Diagnostics.Process.Start(link);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        
        public System.Drawing.Image LoadImageWithReferer(System.String ImageUrl, System.String Referer)
        {
            try
            {
                System.Net.WebClient WebClient = new System.Net.WebClient();
                WebClient.Headers.Add(System.Net.HttpRequestHeader.Referer, Referer);
                WebClient.Headers.Add(System.Net.HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
                System.Byte[] ImageBytes = WebClient.DownloadData(ImageUrl);
                WebClient.Dispose();

                System.IO.MemoryStream ImageMemoryStream = new System.IO.MemoryStream(ImageBytes);
                System.Drawing.Image Image = System.Drawing.Image.FromStream(ImageMemoryStream);
                ImageMemoryStream.Close();
                ImageMemoryStream.Dispose();
                return Image;

            }
            catch (Exception Exception)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }

        }

    }
}
